import os
import sys, datetime, subprocess
from django.core.management import setup_environ
import settings, csv, time
import MySQLdb as mdb
from django.utils.encoding import DjangoUnicodeDecodeError
setup_environ(settings)
from projects.models import *
from  django.db.utils import IntegrityError
from subprocess import CalledProcessError
from django.core.mail import send_mail
from django.template import Context, Template, loader

from archives.models import *

import re, random
from django.template.defaultfilters import slugify
import subprocess, os, time

from projects.models import *
import requests, csv


from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import get_connection

SERVER_IP="10.4.1.51" # CHANGE THIS



import json, requests

def check_exists(path):
    url= "http://10.4.1.182:8002/check_exists/" + path
    try:
        x=requests.get(url)
        js= x.json()
        if js.get("exists", None)==1:
            return True
        else:
            return False
    except:
        return False

def get_files(path):
    url= "http://10.4.1.182:8002/get_files/" + path
    try:
        x=requests.get(url)
        js= x.json()
        if js.get("count", None)!=None:
            return js.get("count")
        else:
            return 0
    except:
        return 0



def get_size(path):
    url= "http://10.4.1.182:8002/get_size/" + path
    try:
        x=requests.get(url)
        js= x.json()
        if js.get("size", None)!=None:
            return js.get("size")
        else:
            return 0
    except:
        return 0

def exists( path):
    try:
        i,p =path.split(SERVER_IP +":")
        url="http://%10.4.1.182:8002/check_exists/" % SERVER_IP
        url =url+p
        try:
            x=requests.get(url)
            j=x.json()
            if int(j.get("exists"))==1:
                return True
            else:
                return False
        except:
            return False
    except:
        return False 

def last_jobs():
    server = Server.objects.get(ipaddress=SERVER_IP)
    projects =Project.objects.filter(legacy=False)
    filename = "project_reports.csv"
    file_path = os.path.join("/var/www/newgen2/csvs/", filename)
    today = datetime.datetime.today()
    if len(projects)<1:
        print "No Jobs"
    
    with open(file_path, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',')
        spamwriter.writerow(["ID", "Source", "Target","Exists", "ITitle",  "No Of Files", "Size in MBs", "Creation Time", "Finalization Time", "Last Update"])
	
	for i in projects:
            print(i.id)
 	    spamwriter.writerow([str(i.id), i.source, str(i.target_path),  str(i.exists), i.ititle, str(i.no_of_files), str(i.size), str(i.time_creation), str(i.time_finalization ), str(i.time_last_update)])
	
    text_content ="Please find Updated Projects %s"  % (str("Whole"))
    subject ="%s -- Project Details" % str(today)
    #print "sub text %s " % email
    #print "html conte"
    from_email = "%s <%s>" % ("Backup Admin", settings.DEFAULT_FROM_EMAIL)
    msg = EmailMultiAlternatives(subject, text_content, from_email, ['ramdas@newgen.co', 'systemsu4@newgen.co'])
    #msg.extra_headers["reply-to"] = 'ramdas@newgen.co'
    
    part = MIMEBase('application', "octet-stream")
    part.set_payload(open( file_path, "rb").read())
    Encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="%s"' % filename)
    msg.attach(part)
    
    
		    
    connection =get_connection(username=settings.EMAIL_HOST_USER,password=settings.EMAIL_HOST_PASSWORD, fail_silently=True)
    print "Connected"
    connection.host =settings.EMAIL_HOST 
    connection.port = 25
    
    msg.connection = connection
    msg.send()
    print "Sent this Email"
    #print "progress marked"
    
    
if __name__=="__main__":
    last_jobs()


